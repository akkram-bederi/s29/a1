const express = require('express');
const app = express()	

const PORT = 3006;


app.use(express.json())
app.use(express.urlencoded({extended:true}))

let users=[{
	"username":"akkram",
	"password":"bederi"
	}
]
app.get("/", (req, res) => res.status(202).send("Hello to the home page") )

app.get("/users", (req, res) => res.status(202).send(users))

app.delete("/delete-user", (req, res) => {
	const{username}=req.body
	res.send(`User ${username} has been deleted`)
})

app.listen(PORT, () => console.log(`Server connected to port ${PORT}`))

